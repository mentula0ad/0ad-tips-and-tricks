# Section one - Interface

## Use groups

**Importance: 5/5 - Skills required: low**

Using groups is what really makes the difference! This is perhaps the most important improvement for the big leap. The game provides 10 group slots, numbered from 0 to 9; use them (possibly all of them). Some groups can be reserved for units, some for buildings producing units, some for buildings providing technologies.

For example, one can assign each of the following categories to a different group:

1. The main Civic Center (so that, by double-pressing the corresponding group key, the camera will directly jump to it)
2. Barracks
3. Stables
4. Arsenals and elephant stables
5. Technological buildings (storehouses, farmsteads, blacksmiths and fortresses)
6. Infantry army
7. Cavalry army

As soon as you place the basement for a new building, immediately add it to the corresponding group, so you don't forget!

**Extra tip**

Put all technological buildings in one single group. Technological buildings are storehouses, farmsteads, blacksmiths and fortresses; in other words, they are all buildings providing economic or military upgrades. By calling this group, you will have an overview on all possible upgrades that you can buy and you can plan a better management of resources.

## Use cameras

**Importance: 1/5 - Skills required: low**

The game provides four different cameras that you can set. A camera allows to jump instantly to a specific location on the map, reducing time spent on navigating the map. It is advisable to rebind the default hotkeys (from `F5` to `F8`) to some more useful keys.

For example, one can use `CapsLock+WASD` for the four cameras, so that fingers never move away from `WASD`.

**Extra tip**

It is not necessary to set a camera on your main Civic Center if you already have the Civic Center in a group: by double-pressing the key corresponding to that group, the camera will instantly focus on it.

## Use ALL keys from the left side of the keyboard

**Importance: 4/5 - Skills required: medium**

Some of the keys that can be pressed with the left hand (such as `Tab` or `CapsLock`) can be reassigned to commands that are frequently used. On the other hand, some very useful keys (such as `I` for selecting idle units, `O` for selecting wounded units or `.` for focusing on idle units) are located on the right part of the keyboard, making them difficult to be reached with the left hand.

It is a good idea to spend some time on finding a good key mapping and get confidence with it.

A recommended configuration is using `CapsLock` to select inactive units (instead of `I`) and `Tab` to select wounded units only (instead of `O`).

**Extra tip**

Bind the *selection.idleworker* hotkey (by default assigned to `.`) to some key you can frequently and easily press, like the middle mouse button; press this key frequently during the game. It is extremely important to keep units always at work; hence it is necessary to constantly check if some units are idle and send them back to work.

## Take a quick look around when players are joining

**Importance: 1/5 - Skills required: low**

It might take a few seconds before all players join an online game. You can use those seconds to navigate the neighborhood of your city, looking for extra berries, animals or other resources.

**Extra tip**

You can quickly navigate the map using the *camera.pan* hotkey, which is by default assigned to the mouse middle button (and you probably might want to rebind it, so that the mouse middle button is used for a better scope); navigating with the pan is faster than navigating using `WASD`.

## Set the unit batch size to 1

**Importance: 4/5 - Skills required: low**

Changing the batch size continuosly during the game can waste precious time. Very often we don't have enough resources to produce the exact amount of units matching the batch size and we must reduce the batch size; or maybe we have too many resources and we must increase the batch size to produce more units. This operation of changing the batch size using the mouse wheel is time consuming.

A good solution is to set the unit batch size to 1. This way, we can produce the highest possible number of units by repeatedly clicking on the unit icon while `Shift` is pressed until the unit icon becomes red.

This method of keeping the batch size set to 1 enhances the management of resources, as it allows to produce as many units as our resources permit.

**Extra tip**

You can use the bottom row of your keyboard to produce units instead of clicking on the unit buttons. For example, with the batch size set to one, you can produce a batch of female citizens from the Civic Center by holding `Shift` and repeatedly pressing `Z`.

## Rebind the *session.kill* hotkey

**Importance: 4/5 - Skills required: low**

Nobody wants their own buildings, and especially the Civic Center, captured by the enemy, right?

When enemy units are trying to capture one of your buildings, you can destroy it. The default key assigned to this command is `Del` (or `Shift+Del` to destroy without confirmation).

However, the `Del` key is very far from your left hand and very often you can't destroy buildings on time, causing severe consequences.

**Extra tip**

Bind the *session.kill* hotkey for unit/building destruction to one of your mouse side keys. This key is one of the "crucial" keys that have to be pressed instantly when needed.

# Section two - Logistics

## Build barracks as close to the wood as possible

**Importance: 3/5 - Skills required: low**

Freshly trained military units should reach the closest resource by traveling the minimum possible distance. A good solution is to build barracks close to a forest or a quarry/mine.

Barracks can also be used to expand your own territory, to reach previously unavailable stone quarries, metal mines or extra berries.

**Extra tip**

Build the first barrack not too far from the Civic Center and not too far from a woodline. This will protect your units and your barrack from early raids.

## Build with women, if you can

**Importance: 1/5 - Skills required: low**

Soldiers are more efficient in gathering resources like wood, stone or metal. When using gatherers to place new buildings, it is preferable to use women, so to get the maximum efficiency out of it.

**Extra tip**

The hotkey *selection.nonmilitaryonly* (by default assigned to `Alt+Y`) allows to select units ignoring soldiers.

## Parallelize house building

**Importance: 2/5 - Skills required: medium**

The game is designed so that building many structures in parallel takes less time than building them one after another. The same applies to houses. Having two or more units building a house each takes less time than having the same amount of units building a house, then moving to the next house and so on...

For this reason, it is very convenient to construct a new house with a single unit, rather than constructing the same house with many units.

This requires some experience in time management, to make sure that we never forget to build houses!

**Extra tip**

When you decide to place a new house using wood gatherers, preferably select women (instead of soldiers) to build it, so that soldiers can gather resources more efficiently during the time the house is built.

## One woman per berry bush

**Importance: 1/5 - Skills required: low**

Berry bushes (as well as other sources of food like date palms or fish) slowly regenerate over time. Assigning one gatherer to each bush guarantees the maximum amount of food regenerated. 

**Extra tip**

Berry bushes, as well as apple trees, fig trees or date palms, accept 8 workers at most. Having more than 8 gatherers might lead to idle units when all but one bushes/trees are exhausted. With a maximum of 8 gatherers we ensure that the job will be finished by all workers simultaneously.

## Leave some space between storehouse and quarry/mine

**Importance: 2/5 - Skills required: low**

One might think that building a storehouse adjacent to a quarry or a mine will cause units travelling less distance to drop the resource. This has in fact the opposite effect. A storehouse which is too close to the resource might occupy the space that units would occupy otherwise. Also, leaving some space between the storehouse and the resource avoids "traffic congestion" around the quarry/mine.

**Extra tip**

Rotate the storehouse if needed, to minimize the distance from different resources.

## Production queues must never be empty

**Importance: 5/5 - Skills required: low**

This is the first commandment of 0AD. It is quite self-explanatory.

When the production queue is full at every building, it is probably the moment to consider building a new production building (e.g. a barrack). Viceversa, it is disadvisable to build a barrack when the production queues are not fully running.

**Extra tip**

Using groups is the key for an efficient unit production management. Frequently click the key corresponding to the group of barracks (or stables or other buildings producing units) to make sure that production queues are always active and running.

## Having idle units is a crime

**Importance: 5/5 - Skills required: low**

Make sure none of your units is idle. There is no "if" or "unless".

**Extra tip**

An icon on the right-bottom of the mini-map indicates the presence/absence of idle units. However, moving the eyes from the middle of the screen to the mini-map and possibly clicking on the icon is tiring and time consuming. Consider rebinding the *selection.idleworker* hotkey (by default assigned to `.`) to some easily accessible key, like the mouse middle button. Keep a finger on that key and press it frequently during the game to detect the presence of idle units and focus instantly on them.

## Put orders in queue after building

**Importance: 5/5 - Skills required: low**

What happens when units finish to build? Deciding once the building is done is one of the many sources of inefficiency. As a general rule it is a good idea to end a queue of orders with resource collection.

For example, if you command your units to build a barrack, you can send them to gather wood after they have finished. Use `Shift` for queuing orders.

## Prepare the basement of any building in advance

**Importance: 3/5 - Skills required: low**

Pick the following example: a batch of five women is about to be trained from the civic center; those women are supposed to build a field. It is convenient to prepare the field before the five women are out and set the rally point of the civic center on the field. Doing so, the women will directly build the field and we save potential idle time.

The same argument applies in general: prepare the basement of fields, storehouses, farmsteads etc. in advance.

**Extra tip**

Women gathering food in fields are perfect for placing buildings. Since food from fields is a never-ending task, gatherers can put new buildings down as a queued order (with the `Shift` key) and never actually move away to build them.

## The rally point of any building must always point to something

**Importance: 4/5 - Skills required: low**

What will units do after being trained? If we don't have an answer to this question, we risk to produce units that remain idle after being produced. Any building (and expecially the new ones) need to have a rally point that focuses on something, for example resources to collect.

**Extra tip**

After the basement of a building is placed, set the rally point of that building, so you don't forget!

# Section three - Fighting

## Evaluate the outcome of a fight

**Importance: 4/5 - Skills required: high**

Evaluating the outcome of a fight requires some experience. There are (at least) three things to take into account:
- the army size: though it might look the most important parameter, it is generally not. The army composition is much more important to determine the outcome of a battle. However, the army size has its own importance.
- the army composition: this parameter is very important. If an army made of swordsmen is outnumbered by an army of skirmishers, it will probably take the upper hand despite the inferior size. For example, it is well known that pikemen are hard to kill and that skiritai unis inflict high damage. If your army consists of units of one type only (for example, only skirmishers or only spearmen), the odds are generally against you. Find the balance that better suits you.
- the army firepower: blacksmith technologies (and the "Will to fight" upgrade) make the difference in a battle.

**Extra tip**

In a battle, click on different types of enemy units to see their stats and check whether they have been strengthen by military technologis and to what extent.

## Don't fight battles you can't win

**Importance: 5/5 - Skills required: medium**

This is a general rule that has to be followed (there are exceptions, like: you strategically want to slow down or distract your enemy).

Deciding whether a battle will turn into a victory or a defeat is hard and requires experience. If you are not at that skill level, just make sure you don't send your units to suicide. Saving units is good!

**Extra tip**

Running from a battle is not a shame. Withdrawing from a battle can be a viable strategic option in many situations. Make sure to have an area of your own territory with enough barracks/temples/fortresses that your units can safely garrison while you prepare for a new battle.

## Use formations

**Importance: 3/5 - Skills required: medium**

The use of formations can be determinant in a battle. In fact, ranged units generally walk faster and have a weaker defence compared to melee units. This frequently causes ranged units to reach the battleground and get killed before melee units arrive in support. Keeping units in formation forces all units to move at the same pace.

There is another important reason why keeping units in formation is good: having melee units on the front and ranged units on the back keeps your army alive for longer time. In fact, melee units are heavily armoured and they can absorbe more damage and thus, stay alive for longer time. In the meantime, ranged units can hit from the back and stay protected.

**Extra tip**

When in doubt, use the "box" formation, or the one that puts melee units on the front and ranged units on the back.

## Manually target multiple units

**Importance: 3/5 - Skills required: high**

By default, units are designed to attack the closest unit. When two armies face each other, it frequently happens that many of your units attack one enemy unit only (the one in the front). This way, many of your units will inflict a lot of damage to one single unit, causing an "overkill". Many of your hits are simply wasted.

It is much more convenient to distribute the damage inflicted by your units. This is particularly relevant when your units are ranged infantry or cavalry. To distribute the damage, keep the `Alt` key pressed and repeatedly right-click on different enemy units to target. The `Alt` key will dispatch one unit of your army to the selected target.

**Extra tip**

In the hectic phase of damage distribution it might happen (and it will happen!) that you right-click on buildings or fields. Make sure to target units only.

## Kill ranged enemy units first

**Importance: 3/5 - Skills required: high**

Ranged units generally deal more damage than melee units. For this reason, when two armies face each other on the battlefield, it is advisable to kill ranged enemy units first.

Be aware that melee units stand in the front of the enemy army, attracting all the damage you inflict. Therefore, targeting ranged units first is almost impossible, unless you manually order your troops to do so. Manually distribute damage across enemy ranged units keeping the `Alt` key pressed and repeatedly right-clicking on different enemy ranged units.

This requires a certain level of micro-management, but it ensures the victory in many cases.

**Extra tip**

Focus on targeting wounded enemy units first; this will reduce the size of the enemy army faster. 

## Use heroes at your own advantage

**Importance: 4/5 - Skills required: medium**

Heroes can give outstanding bonuses that can turn a battle into your favour. Many heores have an *aura* that affects all units/buildings within a certain range. Keep heroes with aura close to the units to take advantage of their bonus. Heroes without an aura can safely stay away from the battle.

**Extra tip**

Put your heroes with aura (especially if they are melee units) in *Passive* mode and keep them on the rear lines during a fight, to prevent them diving into the battle and risk their life. Doing so, your units will benefit of the hero bonus for longer time.
